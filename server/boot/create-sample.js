'use strict';

module.exports = function (app) {
  var ds = app.dataSources.mysqlDS;

  ds.automigrate(function (err, props) {
    console.log('Automigrate complete');
    if (err) throw err;

    function createClient(name, account, password, email) {
      app.models.Client.create({
        name: name,
        username: account,
        password: password,
        email: email,
      }, function (err, client) {
        console.log('created client', client.name);
      })
    }

    function createTipp(goals1, goals2, partieId) {
      app.models.Tipp.create({
        goals1: goals1,
        goals2: goals2,
        partieId: partieId,
        clientId: 1
      }, function (err, tipp) {
        console.log('created tipp', tipp.goals1, tipp.goals2);
      })
    }

    function createGruppe(gruppenName, teams, partien) {
      app.models.Gruppe.create({
        name: gruppenName
      }, function (err, gruppe) {
        teams.forEach(function (team) {
          createTeam(team, gruppe.id);
        });
        if (partien !== undefined) {
          partien.forEach(function (partie, index) {
            createPartie(gruppe.id, partie[0], partie[1], partie[2], partie[3], partie[4])
          });
        };
        // createPartie(gruppe.id, 1, 2);
        // createPartie(gruppe.id, 3, 4);
        // createPartie(gruppe.id, 1, 3);
        // createPartie(gruppe.id, 2, 4);
        // createPartie(gruppe.id, 1, 4);
        // createPartie(gruppe.id, 2, 3);
      });
    }

    function createTeam(teamName, gruppeId) {
      var team = {
        name: teamName,
        gruppeId: gruppeId,
      };
      app.models.Team.create(team, function (err, team) {
        console.log('created team', team.name);
      })
    }

    function createPartie(gruppeId, team1Id, team2Id, goals1 = 0, goals2 = 0, tipp) {
      var partie = {
        gruppeId,
        goals1,
        goals2,
        team1Id,
        team2Id
      }
      app.models.Partie.create(partie, function (err, partie) {
        if (tipp !== undefined) {
          console.log('created Partie with id ', partie.id, '; tipp: ', tipp[0], tipp[1]);
          createTipp(tipp[0], tipp[1], partie.id);
        }
      });
    }

    /* --------------------------------------------------------------------
     * 2018
     * -------------------------------------------------------------------- */
    // createGruppe('Gruppe 1', ['Ägypten', 'Russland', 'Saudi-Arabien', 'Uruguay']);
    // createGruppe('Gruppe 2', ['Iran', 'Marokko', 'Portugal', 'Spanien']);
    // createGruppe('Gruppe 3', ['Australien', 'Dänemark', 'Frankreich', 'Peru']);
    // createGruppe('Gruppe 4', ['Argentinien', 'Island', 'Kroatien', 'Nigeria']);
    // createGruppe('Gruppe 5', ['Brasilien', 'Costa Rica', 'Schweiz', 'Serbien']);
    // createGruppe('Gruppe 6', ['Deutschland', 'Mexiko', 'Schweden', 'Südkorea']);
    // createGruppe('Gruppe 7', ['Belgien', 'England', 'Panama', 'Tunesien']);
    // createGruppe('Gruppe 8', ['Japan', 'Kolumbien', 'Polen', 'Senegal']);

    /* --------------------------------------------------------------------
     * 2006
     * -------------------------------------------------------------------- */
    createGruppe('Gruppe 1', ['Deutschland', 'Costa Rica', 'Polen', 'Ecuador'],
      [[1, 2, 4, 2, [2, 1]], [3, 4, 0, 2, [1, 2]], [1, 3, 1, 0, [2, 2]], [4, 2, 3, 0, [2, 0]], [4, 1, 0, 3, [2, 1]], [2, 3, 1, 2, [1, 3]]]);
    createGruppe('Gruppe 2', ['Trinidad/Tobago', 'Schweden', 'England', 'Paraguay'],
      [[1, 2, 0, 0, [1, 3]], [3, 4, 1, 0, [2, 0]], [1, 3, 0, 2, [1, 1]], [4, 2, 0, 1, [0, 0]], [4, 1, 2, 0, [1, 2]], [2, 3, 2, 2, [3, 2]]]);
    createGruppe('Gruppe 3', ['Argentinien', 'Elfenbeinküste', 'Serbien/Montenegro', 'Holland'],
      [[1, 2, 2, 1, [3, 0]], [3, 4, 0, 1, [1, 2]], [1, 3, 6, 0, [1, 1]], [4, 2, 2, 1, [1, 2]], [4, 1, 0, 0, [0, 3]], [2, 3, 3, 2, [1, 0]]]);
    createGruppe('Gruppe 4', ['Angola', 'Portugal', 'Mexiko', 'Iran'],
      [[1, 2, 0, 1, [0, 2]], [3, 4, 3, 1, [3, 0]], [1, 3, 0, 0, [0, 2]], [4, 2, 0, 2, [1, 1]], [4, 1, 1, 1, [1, 0]], [2, 3, 2, 1, [3, 2]]]);
    createGruppe('Gruppe 5', ['USA', 'Tschechien', 'Italien', 'Ghana'],
      [[1, 2, 0, 3, [1, 2]], [3, 4, 2, 0, [3, 1]], [1, 3, 1, 1, [0, 2]], [4, 2, 2, 0, [2, 1]], [4, 1, 2, 1, [2, 1]], [2, 3, 0, 2, [0, 0]]]);
    createGruppe('Gruppe 6', ['Australien', 'Japan', 'Brasilien', 'Kroatien'],
      [[1, 2, 3, 1, [2, 3]], [3, 4, 1, 0, [3, 0]], [1, 3, 0, 2, [1, 4]], [4, 2, 0, 0, [0, 1]], [4, 1, 2, 2, [0, 2]], [2, 3, 1, 4, [1, 2]]]);
    createGruppe('Gruppe 7', ['Südkorea', 'Togo', 'Frankreich', 'Schweiz'],
      [[1, 2, 2, 1, [1, 2]], [3, 4, 0, 0, [3, 0]], [1, 3, 1, 1, [0, 0]], [4, 2, 2, 0, [1, 1]], [4, 1, 2, 0, [0, 1]], [2, 3, 0, 2, [2, 1]]]);
    createGruppe('Gruppe 8', ['Spanien', 'Ukraine', 'Tunesien', 'Saudi-Arabien'],
      [[1, 2, 4, 0, [3, 0]], [3, 4, 2, 2, [0, 0]], [1, 3, 3, 1, [3, 1]], [4, 2, 0, 4, [0, 2]], [4, 1, 0, 1, [2, 2]], [2, 3, 1, 0, [1, 0]]]);

    createClient('Mats', 'mats', 'mats', 'mats@web.de');
    createClient('Clemens', 'clemens', 'clemens', 'clemens@web.de');

    app.models.Team.findOne({ where: { name: 'Deutschland' } }, function (err, teams) {
      console.log('bin drin', JSON.stringify(teams, null, 2));
      if (err) return next(err);
      // teams.forEach(function (team) {
      //   console.log(`found team ${team.name}`)
      // });
    });
  });
};

